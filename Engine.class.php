<<?php
/**
 *
 * Engine Class for rendering .tpl Files
 *
 * @author Nergon
 * @version 1.0
 *
 */


class Engine {


    private $rawContent;

    public function __construct($file)
    {
        $this->load($file);
    }


    public function load($fileName) {

        if(file_exists("templates/".$fileName)) {
            $this->rawContent = file_get_contents("templates/".$fileName);
        }

        $this->parseFunctions();

    }

    public function parseFunctions() {
        $preg = "/{include file=\"(.*)\.(.*)\"}/";
        while(preg_match($preg , $this->rawContent) === 1)
        {

            $this->rawContent = preg_replace_callback($preg,
                function ($matches) {
                foreach ($matches as $match) {
                    $match2 = str_replace("{include file=\"", "",$match);
                    $match2 = str_replace("\"}","",$match2);
                    return file_get_contents("templates/".$match2);
                }

                },
                $this->rawContent );
        }
    }

    public function assign($string, $replacement) {
        $this->rawContent = str_replace("{\$".$string."}", $replacement, $this->rawContent);
    }


    public function renderTemplate() {
        echo $this->rawContent;
    }




}